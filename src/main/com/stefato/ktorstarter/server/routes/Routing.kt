package com.stefato.ktorstarter.server.routes

import com.stefato.ktorstarter.domain.repository.ProductRepository
import com.stefato.ktorstarter.domain.repository.UserRepository
import io.ktor.routing.Routing

fun Routing.setup(userRepository: UserRepository, productRepository: ProductRepository) {
    index()
    users(userRepository)
    products(productRepository)
}
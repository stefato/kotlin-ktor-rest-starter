package com.stefato.ktorstarter.server.routes

import com.stefato.ktorstarter.server.config.UserAlreadyExistsException
import com.stefato.ktorstarter.domain.model.Product
import com.stefato.ktorstarter.domain.repository.ProductDetails
import com.stefato.ktorstarter.domain.repository.ProductRepository
import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.*

fun Routing.products(repo: ProductRepository) {

    route("/products") {

        get {
            call.respond(repo.fetchAllProducts())
        }


        authenticate {

            post {
                val post = call.receive<ProductDetails>()

                val id = repo.saveProduct(ProductDetails(post.name, post.price)) ?: throw UserAlreadyExistsException()

                val insertedProduct = repo.fetchProduct(id)?: Product()

                call.respond(HttpStatusCode.Created,insertedProduct)
            }
        }



        route("/{id}") {

            get {
                val id = call.parameters["id"]?.toInt()
                val order = repo.fetchProduct(id ?: -1)

                if (order != null) call.respond(order)
                else call.respond(HttpStatusCode.NotFound)
            }

            authenticate {

                delete {
                    val id = call.parameters["id"]?.toInt()

                    if (id == null || !repo.deleteProductById(id)) call.respond(HttpStatusCode.NotFound)

                    call.respond(HttpStatusCode.OK)
                }

                put {
                    val id = call.parameters["id"]?.toInt()
                    val post = call.receive<ProductDetails>()

                    if (id == null || !repo.updateProduct(
                            Product(
                                id,
                                post.name,
                                post.price
                            )
                        ))
                        call.respond(HttpStatusCode.NotFound)

                    call.respond(HttpStatusCode.OK)
                }
            }
        }


    }

}




package com.stefato.ktorstarter.server.routes

import com.stefato.ktorstarter.server.config.InvalidCredentialsException
import com.stefato.ktorstarter.server.config.UserAlreadyExistsException
import com.stefato.ktorstarter.domain.model.Role
import com.stefato.ktorstarter.domain.model.User
import com.stefato.ktorstarter.domain.repository.UserDetails
import com.stefato.ktorstarter.domain.repository.UserRepository
import com.stefato.ktorstarter.server.config.security.JwtConfig
import com.stefato.ktorstarter.server.config.security.PwHashService
import io.ktor.application.call
import io.ktor.auth.UserPasswordCredential
import io.ktor.auth.authenticate
import io.ktor.auth.principal
import io.ktor.features.BadRequestException
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.post

fun Routing.users(repo: UserRepository) {

    authenticate {
        get(path = "/users") {
            checkCredentials(call.principal()!!)
            call.respond(repo.fetchUsers())
        }


        post("/users") {
            // principal should never be null if authentication succeeds and authenticate(optional=false){}
            checkCredentials(call.principal()!!)

            val post = call.receive<UserRequest>()

            //GSON deserialization with non-nullable Kotlin type
            //https://github.com/ktorio/ktor/issues/901
            if (post.name == null || post.pw == null || post.role == null) throw BadRequestException("Bad Request")

            val userExists = !repo.createUser(
                UserDetails(
                    post.name,
                    PwHashService.hashPassword(post.pw),
                    post.role
                )
            )

            if (userExists) throw UserAlreadyExistsException()

            call.respond(HttpStatusCode.Created)

        }


    }

    post("/authorize") {
        val post: UserPasswordCredential
        try {
            post = call.receive()
        } catch (e: Exception) {
            throw BadRequestException("Bad Request")
        }

        //GSON deserialization with non-nullable Kotlin type
        //https://github.com/ktorio/ktor/issues/901
        if (post.name == null || post.password == null) throw BadRequestException("Bad Request")

        val user = repo.fetchUserByName(post.name)

        if (user == null || !PwHashService.checkPassword(post.password, user.pwHash))
            throw InvalidCredentialsException()

        call.respond(mapOf("token" to JwtConfig.makeToken(user)))
    }


}


fun checkCredentials(principal: User) {
    if (principal.role != Role.ADMIN) throw InvalidCredentialsException()
}


data class UserRequest(val name: String, val pw: String, val role: Role)
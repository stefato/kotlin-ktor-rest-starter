package com.stefato.ktorstarter.server.config.security

import com.stefato.ktorstarter.domain.model.Role
import io.ktor.auth.Principal

data class UserPrincipal(val id: Int, val name: String, val pwHash: String, val role: Role): Principal

package com.stefato.ktorstarter.server.config.security

import org.mindrot.jbcrypt.BCrypt
import java.security.MessageDigest
import java.util.*

object PwHashService {

    //thread safe
    private val base64encoder = Base64.getEncoder()

    fun checkPassword(plaintext: String, password: String): Boolean {
        return try {
            BCrypt.checkpw(plaintext.sha512(), password)
        } catch (e: Exception) {
            false
        }
    }

    //bcrypt truncates password at 72 bytes
    //hash pw first to allow longer passwords
    //sha512 88 bytes base64 encoded hash
    fun hashPassword(password: String): String = BCrypt.hashpw(password.sha512(), BCrypt.gensalt())

    //https://security.stackexchange.com/a/61602
    //https://blog.ircmaxell.com/2015/03/security-issue-combining-bcrypt-with.html
    //Base64 encode result to avoid 0x00 bytes
    private fun String.sha512() = MessageDigest.getInstance("SHA-512")
        .digest(toByteArray()).toBase64()

    private fun ByteArray.toBase64() = base64encoder.encodeToString(this)
}

fun main() {
    println(PwHashService.hashPassword("test"))

    /*  val pw = "test"

      val salt = BCrypt.gensalt()

      val pwhash1 = pw.sha512()
      val pwhash2 = pw.sha512().substring(0..71)

      println(pwhash1)
      println(pwhash2)

      println(BCrypt.hashpw(pwhash1, salt))
      println(BCrypt.hashpw(pwhash2, salt))*/
}




package com.stefato.ktorstarter.server.config.security

import com.stefato.ktorstarter.domain.model.User
import com.stefato.ktorstarter.domain.repository.UserRepository
import io.ktor.auth.Authentication
import io.ktor.auth.jwt.jwt

fun Authentication.Configuration.setup(userRepository: UserRepository) {
    jwt {
        verifier(JwtConfig.verifier)
        realm = "ktor.io"
        validate {
            it.payload.subject?.let { name -> userRepository.fetchUserByName(name).toPrincipal() }
        }
    }
}

private fun User?.toPrincipal() = if (this != null) UserPrincipal(id, name, pwHash, role) else null

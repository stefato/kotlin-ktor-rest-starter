package com.stefato.ktorstarter.server.config.security

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import com.stefato.ktorstarter.domain.model.User
import java.util.*

object JwtConfig {

    //TODO to conf
    //https://tools.ietf.org/html/rfc7518#section-3.2
    //A key of the same size as the hash output (for instance, 256 bits for
    //"HS256") or larger MUST be used with this algorithm.
    private const val secret = "A2MepDDQJ93dMqmxJJALvKQtCQ2wnRXxuSBiJXNEwoUGKV4BZKmW5SHcrg3MgI6P"
    private const val issuer = "com.stefato"
    private const val validityInMs = 36_000_00 * 4
    private val algorithm = Algorithm.HMAC512(secret) //HS512

    val verifier: JWTVerifier = JWT
        .require(algorithm)
        .withIssuer(issuer)
        .build()

    fun makeToken(user: User): String = JWT.create()
        .withIssuer(issuer)
        .withSubject(user.name)
        .withExpiresAt(getExpiration())
        .sign(algorithm)

    private fun getExpiration() = Date(System.currentTimeMillis() + validityInMs)

}
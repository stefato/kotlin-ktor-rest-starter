package com.stefato.ktorstarter.server.config

import com.google.gson.stream.MalformedJsonException
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.features.BadRequestException
import io.ktor.features.StatusPages
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.util.pipeline.PipelineContext


fun StatusPages.Configuration.setup() {
    exception<NotImplementedError> { call.respond(HttpStatusCode.NotImplemented) }

    exception<InvalidCredentialsException> { exception ->
        errorResponse(HttpStatusCode.Unauthorized,exception)
    }

    exception<BadRequestException> { exception ->
        errorResponse(HttpStatusCode.BadRequest,exception)
    }

    exception<MalformedJsonException> { exception ->
        errorResponse(HttpStatusCode.BadRequest,exception)
    }

    exception<UserAlreadyExistsException> { exception ->
        //200 or 409
        errorResponse(HttpStatusCode.Conflict,exception)
    }

    exception<ProductNotFoundException> { exception ->
        errorResponse(HttpStatusCode.NotFound,exception)
    }
}

private suspend fun PipelineContext<Unit, ApplicationCall>.errorResponse(httpStatusCode: HttpStatusCode, exception:Exception) {
    call.respond(httpStatusCode, mapOf("error" to (exception.message ?: "")))
}


class InvalidCredentialsException(message: String = "Invalid credentials") : RuntimeException(message)

class UserAlreadyExistsException(message: String = "User already exists") : RuntimeException(message)

class ProductNotFoundException(message: String = "Product not found") : RuntimeException(message)

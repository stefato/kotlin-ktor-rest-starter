package com.stefato.ktorstarter.server

import com.codahale.metrics.JmxReporter
import com.stefato.ktorstarter.data.DBService
import com.stefato.ktorstarter.data.repository.ProductDataRepository
import com.stefato.ktorstarter.data.repository.UserDataRepository
import com.stefato.ktorstarter.domain.repository.ProductRepository
import com.stefato.ktorstarter.domain.repository.UserRepository
import com.stefato.ktorstarter.server.config.security.setup
import com.stefato.ktorstarter.server.config.setup
import com.stefato.ktorstarter.server.routes.setup
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.auth.Authentication
import io.ktor.features.CORS
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.StatusPages
import io.ktor.gson.gson
import io.ktor.http.HttpMethod
import io.ktor.metrics.Metrics
import io.ktor.routing.Routing
import org.slf4j.event.Level
import java.util.concurrent.TimeUnit


@kotlin.jvm.JvmOverloads
fun Application.mainModule(testing: Boolean = false) {

    val userRepo = UserDataRepository() as UserRepository
    val productRepo = ProductDataRepository() as ProductRepository

    DBService.connectDB()
    DBService.initDB()

    install(Authentication) { setup(userRepo) }

    //TODO to conf
    install(CallLogging) {
        level = Level.DEBUG
    }

    //TODO typed route urls
    //install(Locations)

    //TODO to conf
    install(CORS) {
        anyHost()
        allowCredentials = true
        header("Authorization")
        method(HttpMethod.Delete)
        method(HttpMethod.Put)
    }

    install(StatusPages) { setup() }

    install(Metrics) {
        JmxReporter.forRegistry(registry)
            .convertRatesTo(TimeUnit.SECONDS)
            .convertDurationsTo(TimeUnit.MILLISECONDS)
            .build()
            .start()
    }

    install(ContentNegotiation) {
        gson { setPrettyPrinting() }
    }

    install(Routing) {
        setup(userRepo, productRepo)
    }

}







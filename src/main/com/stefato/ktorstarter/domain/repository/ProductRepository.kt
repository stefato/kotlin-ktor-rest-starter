package com.stefato.ktorstarter.domain.repository

import com.stefato.ktorstarter.domain.model.Product

interface ProductRepository {

    fun fetchAllProducts(): List<Product>
    fun fetchProduct(id: Int): Product?
    fun saveProduct(productDetails: ProductDetails):Int?
    fun deleteProductById(id:Int):Boolean
    fun updateProduct(product: Product):Boolean

}

data class ProductDetails(val name: String, val price: Double)


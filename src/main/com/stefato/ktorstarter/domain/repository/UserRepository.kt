package com.stefato.ktorstarter.domain.repository

import com.stefato.ktorstarter.domain.model.Role
import com.stefato.ktorstarter.domain.model.User

interface UserRepository {

    fun fetchUserById(id: Int): User?
    fun fetchUserByName(name: String): User?
    fun fetchUsers(): List<User>
    fun deleteUser(user: User)
    fun createUser(userDetails: UserDetails):Boolean
    fun updateUser(user: User)
}

data class UserDetails(val name:String,val pwHash:String,val role: Role)

package com.stefato.ktorstarter.domain.model

import io.ktor.auth.Principal

data class User(val id: Int, val name: String, val pwHash: String, val role: Role):Principal

enum class Role {
    ADMIN,
    MANAGER
}
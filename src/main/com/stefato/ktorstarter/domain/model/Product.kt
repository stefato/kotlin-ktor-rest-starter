package com.stefato.ktorstarter.domain.model

data class Product(val id: Int = -1, val name: String = "", val price: Double = -1.0)
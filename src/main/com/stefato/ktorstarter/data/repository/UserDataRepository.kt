package com.stefato.ktorstarter.data.repository

import com.stefato.ktorstarter.data.dao.UserEntity
import com.stefato.ktorstarter.data.tables.UserAccountTable
import com.stefato.ktorstarter.domain.model.User
import com.stefato.ktorstarter.domain.repository.UserDetails
import com.stefato.ktorstarter.domain.repository.UserRepository
import org.jetbrains.exposed.sql.transactions.transaction

class UserDataRepository : UserRepository {

    override fun fetchUserById(id: Int) = transaction {
        UserEntity.findById(id)?.toUser()
    }

    override fun fetchUserByName(name: String) = transaction {
        UserEntity.find { UserAccountTable.name eq name }.singleOrNull()?.toUser()
    }

    override fun fetchUsers() = transaction { UserEntity.all().map { it.toUser() } }

    override fun deleteUser(user: User) {
        transaction {
            UserEntity.findById(user.id)?.delete()
        }
    }

    override fun createUser(userDetails: UserDetails): Boolean {
        return transaction {
            val userExists = UserEntity.find { UserAccountTable.name eq userDetails.name }.firstOrNull() != null

            if (userExists) return@transaction false

            UserEntity.new {
                name = userDetails.name
                passhash = userDetails.pwHash
                role = userDetails.role
            }

            return@transaction true
        }
    }

    override fun updateUser(user: User) {
        transaction {
            UserEntity.findById(user.id)?.apply {
                name = user.name
                passhash = user.pwHash
                role = user.role
            }
        }
    }

    private fun UserEntity.toUser() =
        User(id.value, name, passhash, role)

}

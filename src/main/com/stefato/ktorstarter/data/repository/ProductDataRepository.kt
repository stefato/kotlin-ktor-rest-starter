package com.stefato.ktorstarter.data.repository

import com.stefato.ktorstarter.data.dao.ProductEntity
import com.stefato.ktorstarter.data.tables.ProductTable
import com.stefato.ktorstarter.domain.model.Product
import com.stefato.ktorstarter.domain.repository.ProductDetails
import com.stefato.ktorstarter.domain.repository.ProductRepository
import org.jetbrains.exposed.sql.transactions.transaction

class ProductDataRepository : ProductRepository {

    override fun fetchAllProducts() =
        transaction { ProductEntity.all().sortedBy { it.id }.map { it.toProduct() } }


    override fun fetchProduct(id: Int) = transaction {
        ProductEntity.findById(id)?.toProduct()
    }

    override fun saveProduct(productDetails: ProductDetails): Int? {


        return transaction {
            val productExists = ProductEntity.find { ProductTable.name eq productDetails.name }.firstOrNull() != null

            if (productExists) return@transaction null

            val product = ProductEntity.new {
                name = productDetails.name
                price = productDetails.price
            }

            return@transaction product.id.value
        }
    }

    override fun deleteProductById(id: Int): Boolean {
        return transaction {
            val dbProduct = ProductEntity.findById(id)

            return@transaction if (dbProduct != null) {
                dbProduct.delete()
                true
            } else false
        }
    }

    override fun updateProduct(product: Product): Boolean {
        return transaction {
            val dbProduct = ProductEntity.findById(product.id)

            return@transaction if (dbProduct != null) {
                dbProduct.name = product.name
                dbProduct.price = product.price
                true
            } else false
        }

    }

    private fun ProductEntity.toProduct() =
        Product(id.value, name, price)
}




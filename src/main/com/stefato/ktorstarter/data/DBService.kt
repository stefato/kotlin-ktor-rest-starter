package com.stefato.ktorstarter.data

import com.stefato.ktorstarter.data.dao.ProductEntity
import com.stefato.ktorstarter.data.dao.UserEntity
import com.stefato.ktorstarter.data.tables.ProductTable
import com.stefato.ktorstarter.data.tables.UserAccountTable
import com.stefato.ktorstarter.domain.model.Product
import com.stefato.ktorstarter.domain.model.Role
import com.stefato.ktorstarter.domain.model.User
import com.stefato.ktorstarter.server.config.security.PwHashService
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

object DBService {
    fun initDB() {

        val products = mutableListOf(
            Product(0, "Apfel", 1.0),
            Product(1, "Birne", 1.5),
            Product(2, "Kiwi", 1.0),
            Product(3, "Banane", 2.0),
            Product(4, "Mango", 3.0),
            Product(5, "Erbeere", 1.0),
            Product(6, "Himbeere", 1.0)
        )

        val users = listOf(
            User(
                0,
                "hans",
                PwHashService.hashPassword("test"),
                Role.ADMIN
            )
        )

        transaction {
            addLogger(StdOutSqlLogger)

            if (!ProductTable.exists()) {
                SchemaUtils.create(ProductTable)

                products.forEach {
                    ProductEntity.new {
                        name = it.name
                        price = it.price
                    }
                }
            }

            if (!UserAccountTable.exists()) {
                SchemaUtils.create(UserAccountTable)
                users.forEach {
                    UserEntity.new {
                        name = it.name
                        passhash = it.pwHash
                        role = it.role
                    }
                }
            }
        }

    }


    fun connectDB() {
        //TODO to conf
        Database.connect(
            "jdbc:postgresql://nas:32774/", driver = "org.postgresql.Driver",
            user = "postgres"
        )
    }

}

package com.stefato.ktorstarter.data.dao

import com.stefato.ktorstarter.data.tables.UserAccountTable
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class UserEntity(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<UserEntity>(UserAccountTable)

    var name by UserAccountTable.name
    var passhash by UserAccountTable.passhash
    var role by UserAccountTable.role
}
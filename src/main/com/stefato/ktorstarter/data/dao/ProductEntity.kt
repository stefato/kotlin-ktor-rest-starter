package com.stefato.ktorstarter.data.dao

import com.stefato.ktorstarter.data.tables.ProductTable
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class ProductEntity(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<ProductEntity>(ProductTable)

    var name by ProductTable.name
    var price by ProductTable.price
}
package com.stefato.ktorstarter.data.tables

import com.stefato.ktorstarter.domain.model.Role
import org.jetbrains.exposed.dao.IntIdTable

//user is reserved keyword in postgres
//don't use name option of IntIdTable(), double quoted names have some restrictions
object UserAccountTable : IntIdTable() {
    val name = varchar("name", 50).primaryKey(0)
    val passhash = varchar("passhash", 60)
    val role = enumeration("role", Role::class)

    init {
        index(true, name)
    }
}
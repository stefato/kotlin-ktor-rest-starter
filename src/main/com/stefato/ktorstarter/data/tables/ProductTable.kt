package com.stefato.ktorstarter.data.tables

import org.jetbrains.exposed.dao.IntIdTable

object ProductTable : IntIdTable() {
    val name = varchar("name", 50)
    val price = double("price")

    init {
        index(true, name)
    }
}